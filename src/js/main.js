/**
 * Created by Angela on 20/03/2016.
 */

var myApp = angular.module('ConversorMoneda', []);

myApp.controller('MainController', [function() {
    this.monedas=[
        {id:0,name:'Euro (EUR)'},
        {id:1,name:'Dolar (USD)'},
        {id:2, name:'Libra Esterlina (GBP)'}
    ];

    this.conversion=[[1,1.2,0.71],[0.83,1,0.59],[1.4,1.68,1]];

    this.convertirMoneda= function(){
        this.resultado=parseFloat(this.numero) * parseFloat(this.conversion[this.origen][this.destino]);
    };
}]);